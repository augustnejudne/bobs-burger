import { createUseStyles } from 'react-jss';

const useStyles = createUseStyles(theme => {
  const {
    palette: { red, white },
  } = theme;
  return {
    burgerContainer: {
      marginTop: 30,
      display: 'flex',
      flexDirection: 'column',
      alignItems: 'center',
    },
    burger: {
      maxWidth: '90%',
    },
    new: {
      '@media (min-width: 600px)': {
        fontSize: 30,
      },
    },
    veggieBurger: {
      color: red,
      fontSize: 30,
      '@media (min-width: 600px)': {
        fontSize: 40,
      },
    },
    burgerContainer2: {
      position: 'relative',
      display: 'flex',
      justifyContent: 'center',
    },
    priceContainer: {
      position: 'absolute',
      bottom: -20,
      right: 20,
      background: red,
      color: white,
      borderRadius: 100,
      width: 100,
      height: 100,
      fontSize: 20,
      display: 'flex',
      justifyContent: 'center',
      alignItems: 'center',
      '@media (min-width: 600px)': {
        fontSize: 30,
        width: 140,
        height: 140,
      },
    },
  };
});

export default useStyles;
