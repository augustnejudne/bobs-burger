import React from 'react';
import burgerHero from '../../assets/img/hero-burger.webp';
import AddToCart from '../ui-components/add-to-cart';
import useStyles from './styles';
import { useTheme } from 'react-jss';

const Hero = props => {
  const theme = useTheme();
  const classes = useStyles(theme);
  const { addToCart } = props;
  return (
    <div className={classes.burgerContainer}>
      <h1 className={classes.new}>NEW!</h1>
      <h2 className={classes.veggieBurger}>Veggie Burger</h2>
      <div className={classes.burgerContainer2}>
        <img className={classes.burger} src={burgerHero} alt="burger" />
        <div className={classes.priceContainer}>
          <h2 className={classes.price}>&#8369;150</h2>
        </div>
      </div>
      <AddToCart onClick={() => addToCart('Veggie Burger', 150)} />
    </div>
  );
};

export default React.memo(Hero);
