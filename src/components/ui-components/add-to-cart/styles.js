import { createUseStyles } from 'react-jss';

const useStyles = createUseStyles(theme => {
  const {
    palette: { red },
  } = theme;
  return {
    container: {
      display: 'flex',
      justifyContent: 'center',
    },
    addToCart: {
      color: red,
      fontSize: 18,
      margin: '10px 0',
      borderBottom: '4px dashed transparent',
      cursor: 'pointer',
      textAlign: 'center',
      '&:hover': {
        borderBottom: `4px dashed ${red}`,
      },
    },
  };
});

export default useStyles;
