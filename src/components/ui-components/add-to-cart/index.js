import React from 'react';
import useStyles from './styles';
import { useTheme } from 'react-jss';

const AddToCart = props => {
  const theme = useTheme();
  const classes = useStyles(theme);
  return (
    <div onClick={props.onClick} className={classes.container}>
      <h3 className={classes.addToCart}>Add to cart</h3>;
    </div>
  );
};

export default React.memo(AddToCart);
