import React from 'react';
import useStyles from './styles';
import { useTheme } from 'react-jss';

const Tabs = props => {
  const theme = useTheme();
  const classes = useStyles(theme);
  const { tab, setTab } = props;
  return (
    <div className={classes.tabsContainer}>
      <div
        onClick={() => setTab(0)}
        className={tab === 0 ? classes.activeTab : classes.tab}
      >
        <h2 className={classes.tabText}>Burgers</h2>
      </div>
      <div
        onClick={() => setTab(1)}
        className={tab === 1 ? classes.activeTab : classes.tab}
      >
        <h2 className={classes.tabText}>Beer</h2>
      </div>
    </div>
  );
};

export default React.memo(Tabs);
