import { createUseStyles } from 'react-jss';

const useStyles = createUseStyles(theme => {
  const {
    palette: { red },
  } = theme;
  return {
    tabsContainer: {
      marginTop: 20,
      display: 'flex',
      justifyContent: 'space-around',
      alignItems: 'center',
    },
    tab: {
      minWidth: 120,
      cursor: 'pointer',
      borderBottom: '5px dashed transparent',
      '&:hover': {
        color: red,
      },
    },
    activeTab: {
      color: red,
      cursor: 'pointer',
      minWidth: 120,
      borderBottom: `5px dashed ${red}`,
    },
    tabText: {
      fontSize: 20,
      textAlign: 'center',
      letterSpacing: 2,
      padding: 10,
      '@media (min-width: 600px)': {
        fontSize: 30,
      },
    },
  };
});

export default useStyles;
