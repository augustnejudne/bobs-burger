import { createUseStyles } from 'react-jss';

const useStyles = createUseStyles(theme => {
  return {
    cartItem: {
      display: 'grid',
      gridTemplateColumns: '2fr 1fr 1fr',
      gridGap: 10,
      alignItems: 'center',
      '& h2': {
        fontFamily: 'Lato, sans-serif',
        fontSize: 18,
        padding: '20px 0',
        textAlign: 'center',
      },
    },
    qty: {
      display: 'grid',
      gridTemplateColumns: 'repeat(3, 1fr)',
      alignItems: 'center',
      textAlign: 'center',
      '& span': {
        cursor: 'pointer',
        fontSize: 40,
        fontWeight: 900,
      },
    },
    name: {
      display: 'grid',
      gridTemplateColumns: '1fr 11fr',
      alignItems: 'center',
    },
    remove: {
      cursor: 'pointer',
      fontSize: 30,
      fontWeight: 100,
    },
  };
});

export default useStyles;
