import React from 'react';
import { numberWithCommas } from '../../utils/functions';
import useStyles from './styles';
import { useTheme } from 'react-jss';


const CartItem = props => {
  const theme = useTheme();
  const classes = useStyles(theme);
  const { item, addOne, subtractOne, removeFromCart } = props;
  return (
    <div className={classes.cartItem}>
      <h2 className={classes.name}>
        <span
          onClick={() => removeFromCart(item)}
          className={classes.remove}>&times;</span>
        {item.name}
      </h2>
      <h2 className={classes.qty}>
        <span onClick={() => subtractOne(item)} className={classes.minus}>
          &#x2212;
        </span>
        {item.qty}
        <span onClick={() => addOne(item)} className={classes.plus}>
          &#43;
        </span>
      </h2>
      <h2>&#8369;{numberWithCommas(item.price * item.qty)}</h2>
    </div>
  );
};

export default React.memo(CartItem);
