import beer1 from '../../assets/img/beers/beer1.webp';
import beer2 from '../../assets/img/beers/beer2.webp';
import beer3 from '../../assets/img/beers/beer3.webp';
import beer4 from '../../assets/img/beers/beer4.webp';
import beer5 from '../../assets/img/beers/beer5.webp';
import beer6 from '../../assets/img/beers/beer6.webp';
import beer7 from '../../assets/img/beers/beer7.webp';
import beer8 from '../../assets/img/beers/beer8.webp';
import beer9 from '../../assets/img/beers/beer9.webp';
import beer10 from '../../assets/img/beers/beer10.webp';

export default [
  {
    id: 1,
    name: 'Bridge Road Brewers',
    image: beer1,
    price: 69,
    desc: 'Drink triple, see double, act single',
  },
  {
    id: 2,
    name: 'Carlton Mid',
    image: beer2,
    price: 75,
    desc: 'Never chase anything but drinks and dreams.',
  },
  {
    id: 3,
    name: 'Hahn Super Dry',
    image: beer3,
    price: 65,
    desc:
      'Hanging with good people, and drinking good beer. No complaints here.',
  },
  {
    id: 4,
    name: 'Little Creatures Rogers',
    image: beer4,
    price: 72,
    desc: 'The best beers are the ones we drink with friends.',
  },
  {
    id: 5,
    name: 'Pure Blonde',
    image: beer5,
    price: 80,
    desc:
      'Beer is made from hops. Hops are plants. Therefore, beer is a salad."',
  },
  {
    id: 6,
    name: 'Colonial Brewing Small Ale',
    image: beer6,
    price: 82,
    desc: 'You better beer-lieve we went to a brewery.',
  },
  {
    id: 7,
    name: 'Great Northern Super Crisp',
    image: beer7,
    price: 85,
    desc: 'Thank brew very much for spending the day with me.',
  },
  {
    id: 8,
    name: 'Pirate Life Throwback',
    image: beer8,
    price: 78,
    desc: `Hello? Is it beer you're looking for?`,
  },
  {
    id: 9,
    name: 'Steersman Ultra Dry',
    image: beer9,
    price: 79,
    desc: 'Sip, sip, hooray with my best friends at the brewery.',
  },
  {
    id: 10,
    name: 'Australian Lager Gold',
    image: beer10,
    price: 89,
    desc: 'Chill for best results.',
  },
];
