import React, { Fragment, useState } from 'react';
import Beer from '../beer';
import beersList from './beers-list';
import { sortBy } from '../../utils/functions';
import useStyles from './styles';
import { useTheme } from 'react-jss';


const BeersList = props => {
  const theme = useTheme();
  const classes = useStyles(theme);
  const { addToCart } = props;
  const [filter, setFilter] = useState('id');
  const burgers = sortBy(beersList, filter);
  return (
    <Fragment>
      <div className={classes.filters}>
        <div className={classes.sort}>Sort</div>
        <div
          onClick={() => setFilter('id')}
          className={filter === 'id' ? classes.filterActive : classes.filter}
        >
          Default
        </div>
        <div
          onClick={() => setFilter('name')}
          className={filter === 'name' ? classes.filterActive : classes.filter}
        >
          Name
        </div>
        <div
          onClick={() => setFilter('price')}
          className={filter === 'price' ? classes.filterActive : classes.filter}
        >
          Price
        </div>
      </div>
      <div className={classes.beersContainer}>
        {burgers.map((b, i) => (
          <Beer addToCart={addToCart} beer={b} key={i} />
        ))}
      </div>
    </Fragment>
  );
};

export default React.memo(BeersList);
