import { createUseStyles } from 'react-jss';

const useStyles = createUseStyles(theme => {
  return {
    mainContainer: {
      maxWidth: 920,
      margin: '0 auto',
      flex: 1,
      minHeight: '90vh',
    },
  };
});

export default useStyles;
