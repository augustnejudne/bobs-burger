import React, { Fragment, useState, useEffect } from 'react';
import { useTheme } from 'react-jss';
import ViewCart from '../view-cart';
import TopBanner from '../top-banner';
import Cart from '../cart';
import Hero from '../hero';
import Tabs from '../tabs';
import BurgersList from '../burgers-list';
import BeersList from '../beers-list';
import AboutUs from '../about-us';
import Footer from '../footer';
import useStyles from './styles';

const App = () => {
  const theme = useTheme();
  const classes = useStyles(theme);

  const [tab, setTab] = useState(0);
  const [cart, setCart] = useState([]);
  const [total, setTotal] = useState(0);
  const [viewCart, setViewCart] = useState(false);

  const addOne = thing => {
    const cartCopy = cart;
    const item = cartCopy.find(i => {
      return i === thing;
    });
    item.qty += 1;
    setCart([...cartCopy]);
  };

  const subtractOne = thing => {
    const cartCopy = cart;
    const item = cartCopy.find(i => {
      return i === thing;
    });
    if (item.qty < 1) return;
    item.qty -= 1;
    setCart([...cartCopy]);
  };

  const addToCart = (name, price) => {
    const alreadyInCart = cart.find(c => {
      return c.name === name;
    });
    if (alreadyInCart) {
      return;
    }
    setCart([...cart, { name, price, qty: 1 }]);
  };

  const removeFromCart = thing => {
    setCart([...cart.filter(i => i !== thing)]);
  };

  useEffect(() => {
    setTotal(
      cart.reduce((a, c) => {
        return a + c.qty * c.price;
      }, 0)
    );
  }, [cart]);

  return (
    <Fragment>
      <div className={classes.mainContainer}>
        {!viewCart && <Cart cart={cart} setViewCart={setViewCart} />}
        <TopBanner />
        {viewCart ? (
          <ViewCart
            cart={cart}
            addOne={addOne}
            subtractOne={subtractOne}
            setViewCart={setViewCart}
            removeFromCart={removeFromCart}
            total={total}
          />
        ) : (
          <Fragment>
            <Hero addToCart={addToCart} />
            <Tabs tab={tab} setTab={setTab} />
            {tab === 0 && <BurgersList addToCart={addToCart} />}
            {tab === 1 && <BeersList addToCart={addToCart} />}
          </Fragment>
        )}
      </div>
      <AboutUs />
      <Footer />
    </Fragment>
  );
};

export default React.memo(App);
