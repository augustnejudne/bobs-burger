import { createUseStyles } from 'react-jss';

const useStyles = createUseStyles(theme => {
  const {
    palette: { grey1, white },
  } = theme;
  return {
    burgersContainer: {
      margin: '30px auto 0',
      display: 'grid',
      gridGap: 20,
      gridTemplateColumns: 'repeat(auto-fit, minmax(320px, 1fr))',
      justifyContent: 'center',
    },
    filters: {
      marginTop: 30,
      display: 'flex',
      justifyContent: 'space-around',
      fontSize: 14,
      '@media (min-width: 400px)': {
        fontSize: 16,
        maxWidth: 300,
        margin: '30px auto 0',
      },
    },
    sort: {
      padding: '4px 8px',
      borderRadius: 20,
      border: '1px solid transparent',
    },
    filter: {
      padding: '4px 8px',
      borderRadius: 20,
      border: `1px solid ${grey1}`,
      cursor: 'pointer',
    },
    filterActive: {
      padding: '4px 8px',
      background: grey1,
      color: white,
      borderRadius: 20,
      cursor: 'pointer',
    },
  };
});

export default useStyles;
