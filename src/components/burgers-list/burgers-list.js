import burger1 from '../../assets/img/burgers/burger1.webp';
import burger2 from '../../assets/img/burgers/burger2.webp';
import burger3 from '../../assets/img/burgers/burger3.webp';
import burger4 from '../../assets/img/burgers/burger4.webp';
import burger5 from '../../assets/img/burgers/burger5.webp';
import burger6 from '../../assets/img/burgers/burger6.webp';
import burger7 from '../../assets/img/burgers/burger7.webp';
import burger8 from '../../assets/img/burgers/burger8.webp';
import burger9 from '../../assets/img/burgers/burger9.webp';
import burger10 from '../../assets/img/burgers/burger10.webp';
import burger11 from '../../assets/img/burgers/burger11.webp';
import burger12 from '../../assets/img/burgers/burger12.webp';
import burger13 from '../../assets/img/burgers/burger13.webp';
import burger14 from '../../assets/img/burgers/burger14.webp';
import burger15 from '../../assets/img/burgers/burger15.webp';
import burger16 from '../../assets/img/burgers/burger16.webp';
import burger17 from '../../assets/img/burgers/burger17.webp';
import burger18 from '../../assets/img/burgers/burger18.webp';

export default [
  {
    id: 1,
    name: 'Stomper Jr.',
    image: burger1,
    desc:
      'Gut busting, best-dressed seared to sizzle-crisp on the outside. Sloppy with chin-dripping juice on the inside.',
    price: 69,
  },
  {
    id: 2,
    name: 'Double Stomper Jr.',
    image: burger2,
    desc:
      'Fire-grilled patty, flame-grilled, charred, seared, well-done. All-natural beef, grass-feed beef. Deliciously moist',
    price: 129,
  },
  {
    id: 3,
    name: 'Stomper Classic',
    image: burger3,
    desc:
      'Life is too short to miss out on beautiful things like a Stomper Classic.',
    price: 155,
  },
  {
    id: 4,
    name: 'Pepperoni Bacon Stomper Jr.',
    image: burger4,
    desc:
      '"I\'m all about fashion, Pepperoni Bacon Stomper Jr., and bright red lipstick." - Scarlett Johansson',
    price: 125,
  },
  {
    id: 5,
    name: 'Pepperoni Bacon Stomper',
    image: burger5,
    desc:
      'I would like a Pepperoni Bacon Stomper, with a side of Pepperoni Bacon Stomper, and see if they can make me a Pepperoni Bacon Stomper smoothie.',
    price: 205,
  },
  {
    id: 6,
    name: '4-Cheese Stomper Jr.',
    image: burger6,
    desc: `I'm into fitness…fitness whole burger in my mouth.`,
    price: 99,
  },
  {
    id: 7,
    name: '4-Cheese Stomper',
    image: burger7,
    desc: 'A burger without cheese is like a hug without a squeeze.',
    price: 185,
  },
  {
    id: 8,
    name: 'Bacon 4-Cheese Stomper Jr.',
    image: burger8,
    desc: 'Now THAT is a tasty burger.',
    price: 164,
  },
  {
    id: 9,
    name: 'Bacon 4-Cheese Stomper',
    image: burger9,
    desc: 'The only bad burger is the one you didn’t eat.',
    price: 260,
  },
  {
    id: 10,
    name: 'Flame-Grilled Cheeseburger',
    image: burger10,
    desc: 'When life throws you a burger, eat it.',
    price: 69,
  },
  {
    id: 11,
    name: 'Flame-Grilled Double Cheeseburger',
    image: burger11,
    desc: 'We followed our hearts and it led us to burgers',
    price: 140,
  },
  {
    id: 12,
    name: 'Flame-Grilled Triple Cheeseburger',
    image: burger12,
    desc: 'A balanced diet is a burger in each hand.',
    price: 200,
  },
  {
    id: 13,
    name: 'Flame-Grilled Quadruple Cheeseburger',
    image: burger13,
    desc: 'This burger has too much cheese, said no one ever.',
    price: 240,
  },
  {
    id: 14,
    name: 'Quarter Pound Duke',
    image: burger14,
    desc: 'A perfectly round quarter pound.',
    price: 165,
  },
  {
    id: 15,
    name: 'BBQ Bacon Cheeseburger',
    image: burger15,
    desc: 'The best burgers are like life — messy and topped with bacon.',
    price: 120,
  },
  {
    id: 16,
    name: 'Double BBQ Bacon Cheesburger',
    image: burger16,
    desc:
      'The best burgers are like life — messy and topped with even more bacon.',
    price: 180,
  },
  {
    id: 17,
    name: 'Flame-Grilled BBQ Hamburger',
    image: burger17,
    desc: 'So many burgers, so little time.',
    price: 64,
  },
  {
    id: 18,
    name: 'Flame-Grilled Hamburger',
    image: burger18,
    desc: 'Lady in the streets, freak when she eats (Flame-Grilled Hamburger).',
    price: 64,
  },
];
