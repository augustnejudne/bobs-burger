import React, { Fragment, useState } from 'react';
import Burger from '../burger';
import burgersList from './burgers-list';
import { sortBy } from '../../utils/functions';
import useStyles from './styles';
import { useTheme } from 'react-jss';

const BurgersList = props => {
  const theme = useTheme();
  const classes = useStyles(theme);
  const { addToCart } = props;
  const [filter, setFilter] = useState('id');
  const burgers = sortBy(burgersList, filter);
  return (
    <Fragment>
      <div className={classes.filters}>
        <div className={classes.sort}>Sort:</div>
        <div
          onClick={() => setFilter('id')}
          className={filter === 'id' ? classes.filterActive : classes.filter}
        >
          Default
        </div>
        <div
          onClick={() => setFilter('name')}
          className={filter === 'name' ? classes.filterActive : classes.filter}
        >
          Name
        </div>
        <div
          onClick={() => setFilter('price')}
          className={filter === 'price' ? classes.filterActive : classes.filter}
        >
          Price
        </div>
      </div>
      <div className={classes.burgersContainer}>
        {burgers.map((b, i) => (
          <Burger addToCart={addToCart} burger={b} key={i} />
        ))}
      </div>
    </Fragment>
  );
};

export default React.memo(BurgersList);
