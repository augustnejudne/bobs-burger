import { createUseStyles } from 'react-jss';

const useStyles = createUseStyles(theme => {
  const { palette: { grey1, red }} = theme;
  return {
    container: {
      margin: '0 5px',
      marginTop: 30,
    },
    top: {
      alignItems: 'center',
      display: 'grid',
      gridTemplateColumns: '3fr 1fr',
      '& h1': {
        textAlign: 'center',
        padding: '0 10px',
      },
      '& span': {
        color: red,
        display: 'flex',
        justifyContent: 'flex-end',
        cursor: 'pointer',
        fontSize: 18,
      },
    },
    head: {
      display: 'grid',
      gridGap: 10,
      gridTemplateColumns: '2fr 1fr 1fr',
      '& h2': {
        marginTop: 10,
        textAlign: 'center',
        paddingBottom: 10,
        borderBottom: `2px dashed ${grey1}`,
      },
    },
    total: {
      display: 'grid',
      gridGap: 10,
      gridTemplateColumns: '3fr 1fr',
      '& h1:nth-child(1)': {
        textAlign: 'right',
        padding: '10px 0',
      },
      '& h1:nth-child(2)': {
        textAlign: 'center',
        padding: '10px 0',
      },
    },
    checkoutContainer: {
      display: 'flex',
      justifyContent: 'center',
    },
    checkout: {
      cursor: 'pointer',
      marginTop: 40,
      textTransform: 'uppercase',
      color: red,
      textAlign: 'center',
      border: '2px dashed transparent',
      padding: '10px 20px',
      '&:hover': {
        border: `2px dashed ${red}`,
      },
    },
  };
});

export default useStyles;
