import React, { useEffect } from 'react';
import CartItem from '../cart-item';
import { numberWithCommas } from '../../utils/functions';
import useStyles from './styles';
import { useTheme } from 'react-jss';

const ViewCart = props => {
  const theme = useTheme();
  const classes = useStyles(theme);
  const {
    cart,
    total,
    setViewCart,
    addOne,
    subtractOne,
    removeFromCart,
  } = props;
  useEffect(() => {
    window.scrollTo(0, 0);
  }, []);
  return (
    <div className={classes.container}>
      <div className={classes.top}>
        <h1>Your Cart</h1>
        <span onClick={() => setViewCart(false)}>
          <h1>&times;</h1>
        </span>
      </div>
      <div className={classes.head}>
        <h2>Item</h2>
        <h2>Qty</h2>
        <h2>Price</h2>
      </div>
      {cart.map((item, i) => (
        <CartItem
          item={item}
          key={i}
          removeFromCart={removeFromCart}
          addOne={addOne}
          subtractOne={subtractOne}
        />
      ))}
      <div className={classes.total}>
        <h1>Total</h1>
        <h1>&#8369;{numberWithCommas(total)}</h1>
      </div>
      <div className={classes.checkoutContainer}>
        <h1 className={classes.checkout}>Checkout</h1>
      </div>
    </div>
  );
};

export default React.memo(ViewCart);
