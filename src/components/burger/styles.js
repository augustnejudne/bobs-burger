import { createUseStyles } from 'react-jss';

const useStyles = createUseStyles(theme => {
  const {
    palette: { red, white },
  } = theme;
  return {
    burgerContainer: {
      marginBottom: 20,
      position: 'relative',
    },
    imgContainer: {
      display: 'flex',
      justifyContent: 'center',
    },
    img: {
      maxWidth: '90%',
    },
    name: {
      textAlign: 'right',
      maxWidth: '60%',
      fontSize: 25,
      position: 'absolute',
      top: 10,
      right: 20,
    },
    price: {
      background: red,
      borderRadius: 100,
      position: 'absolute',
      top: 10,
      left: 10,
      color: white,
      fontSize: 28,
      padding: '10px 20px',
    },
    desc: {
      fontSize: 18,
      textAlign: 'center',
    },
  };
});

export default useStyles;
