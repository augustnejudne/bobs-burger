import { createUseStyles } from 'react-jss';

const useStyles = createUseStyles(theme => {
  const { palette: { white, red  }} = theme;
  return {
    container: {
      position: 'sticky',
      top: 0,
      zIndex: 1,
      display: 'flex',
    },
    cart: {
      background: red,
      color: white,
      padding: '5px 10px',
      fontSize: 18,
      fontWeight: 900,
      marginBottom: -32,
      cursor: 'pointer',
      borderRadius: '0 0 20px 20px',
    },
  };
});

export default useStyles;
