import React, { useState } from 'react';
import useStyles from './styles';
import { useTheme } from 'react-jss';


const Cart = props => {
  const theme = useTheme();
  const classes = useStyles(theme);
  const { cart, setViewCart } = props;
  const [hover, setHover] = useState(false);
  return (
    <div className={classes.container}>
      <div
        onMouseEnter={() => setHover(true)}
        onMouseLeave={() => setHover(false)}
        onClick={() => setViewCart(true)}
        className={classes.cart}
      >
        {hover ? <p>View Cart</p> : <p>Cart: {cart.length}</p>}
      </div>
    </div>
  );
};

export default React.memo(Cart);
