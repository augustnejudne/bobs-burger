import React from 'react';

const Footer = props => {
  return (
    <p
      style={{
        margin: '60px auto 10px',
        textAlign: 'center',
        fontSize: 14,
      }}
    >
      August Nejudne &copy;{new Date().getFullYear()}
    </p>
  );
};

export default React.memo(Footer);
