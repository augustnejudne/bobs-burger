import React from 'react';
import AddToCart from '../ui-components/add-to-cart';
import useStyles from './styles';
import { useTheme } from 'react-jss';

const Beer = props => {
  const theme = useTheme();
  const classes = useStyles(theme);
  const {
    addToCart,
    beer: { name, image, desc, price },
  } = props;
  const handleClick = () => {
    addToCart(name, price);
  };
  return (
    <div className={classes.beerContainer}>
      <div className={classes.imgContainer}>
        <img className={classes.img} src={image} alt={name} />
      </div>
      <div className={classes.textContainer}>
        <h3 className={classes.name}>{name}</h3>
        <h2 className={classes.price}>&#8369;{price}</h2>
        <AddToCart onClick={handleClick} />
        <p className={classes.desc}>{desc}</p>
      </div>
    </div>
  );
};

export default React.memo(Beer);
