import { createUseStyles } from 'react-jss';

const useStyles = createUseStyles(theme => {
  const {
    palette: { white, red },
  } = theme;
  return {
    beerContainer: {
      marginBottom: 20,
      position: 'relative',
    },
    imgContainer: {
      display: 'flex',
      paddingLeft: 30,
    },
    img: {
      height: 300,
      transform: 'rotate(-12deg)',
    },
    textContainer: {
      maxWidth: '70%',
      position: 'absolute',
      top: 40,
      right: 30,
      display: 'flex',
      flexDirection: 'column',
      alignItems: 'flex-end',
    },
    name: {
      fontSize: 30,
      textAlign: 'right',
    },
    price: {
      background: red,
      fontSize: 28,
      padding: '10px 20px',
      borderRadius: 100,
      color: white,
      textAlign: 'right',
    },
    desc: {
      fontSize: 18,
      maxWidth: '50%',
      textAlign: 'right',
    },
  };
});

export default useStyles;
