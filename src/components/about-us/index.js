import React from 'react';
import useStyles from './styles';
import { useTheme } from 'react-jss';

const AboutUs = props => {
  const theme = useTheme();
  const classes = useStyles(theme);
  return (
    <div className={classes.container}>
      <div className={classes.content}>
        <div className={classes.chef} />
        <div className={classes.textContainer}>
          <h1 className={classes.about}>About Bob</h1>
          <p className={classes.p}>
            One bite of a hot, juicy Stomper and suddenly you’re in Dumaguete
            City, 2010. After all, it was in that picturesque, coastal Central
            Visayas town, in the same year that internet memes went mainstream,
            that the original Bob&apos;s Burgers &amp; Beer delivering burgers
            and beer to doorsteps. Soon after, an entrepreneurial young employee
            on a mission to provide a great burger at a great price—borrowed
            money from their mom to buy the unassuming kitchen nestled amongst
            the American Colonial architecture of Silliman Beach and nearby
            bungalow homes.
          </p>
        </div>
      </div>
    </div>
  );
};

export default React.memo(AboutUs);
