import { createUseStyles } from 'react-jss';
import chef from '../../assets/img/chef.webp';

const useStyles = createUseStyles(theme => {
  const {
    palette: { black, white },
  } = theme;
  return {
    container: {
      marginTop: 60,
      background: black,
      color: white,
    },
    content: {
      margin: '0 auto',
      maxWidth: 920,
      padding: 20,
      '@media (min-width: 600px)': {
        display: 'grid',
        gridTemplateColumns: '3fr 5fr',
        alignItems: 'center',
      },
    },
    chef: {
      margin: '0 auto',
      width: 240,
      height: 300,
      background: `url(${chef})`,
      backgroundSize: 'cover',
      backgroundPosition: 'top center',
    },
    about: {
      textAlign: 'center',
      padding: '20px 0',
      marginBottom: 20,
      borderBottom: `2px dashed ${white}`,
      '@media (min-width: 600px)': {
        fontSize: 30
      }
    },
    p: {
      textAlign: 'center',
      fontSize: 14,
      '@media (min-width: 600px)': {
        fontSize: 16 
      }
    },
  };
});

export default useStyles;
