import { createUseStyles } from 'react-jss';

const useStyles = createUseStyles(theme => {
  const {
    palette: { grey1, white },
  } = theme;
  return {
    h1: {
      textAlign: 'center',
      fontSize: 30,
      marginBottom: 0,
      marginTop: 30,
      '@media (min-width: 600px)': {
        fontSize: 40,
      },
    },
    h2Container: {
      margin: '0 auto',
      display: 'flex',
      justifyContent: 'center',
    },
    leftTriangle: {
      width: 30,
      background: '#2e2e2e',
      clipPath: 'polygon(100% 0, 100% 100%, 0% 100%, 50% 50%, 0% 0%)',
      transform: 'translate(12px, -3px)',
      borderRadius: '0 3px 0 0',
    },
    rightTriangle: {
      width: 30,
      background: '#2e2e2e',
      clipPath: 'polygon(100% 0, 50% 50%, 100% 100%, 0 100%, 0 0)',
      transform: 'translate(-12px, -3px)',
      borderRadius: '3px 0 0 0',
    },
    h2: {
      fontSize: 14,
      padding: '6px 12px',
      color: white,
      background: grey1,
      textAlign: 'center',
      margin: 0,
      borderRadius: '0 0 3px 3px',
      '@media (min-width: 600px)': {
        fontSize: 20,
      },
    },
  };
});

export default useStyles;
