import React, { Fragment } from 'react';
import useStyles from './styles';
import { useTheme } from 'react-jss';

const TopBanner = () => {
  const theme = useTheme();
  const classes = useStyles(theme);
  return (
    <Fragment>
      <h1 className={classes.h1}>Bob&apos;s Burgers & Beer</h1>
      <div className={classes.h2Container}>
        <div className={classes.leftTriangle} />
        <p className={classes.h2}>Where the burger is as good as the beer.</p>
        <div className={classes.rightTriangle} />
      </div>
    </Fragment>
  );
};

export default React.memo(TopBanner);
