import React from 'react';
import ReactDOM from 'react-dom';
import App from './components/app';
import { ThemeProvider } from 'react-jss';

import './index.css';

const theme = {
  palette: {
    red: '#f21b2d',
    black: '#0d0d0d',
    white: '#f2f2f2',
    grey1: '#262626',
    grey2: '#888b8c',
  },
  background: '#f7df1e',
  color: '#24292e',
};

ReactDOM.render(
  <React.StrictMode>
    <ThemeProvider theme={theme}>
      <App />
    </ThemeProvider>
  </React.StrictMode>,
  document.getElementById('root')
);
